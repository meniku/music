# Music

Music is a puzzle game about making music. It was made in 8 hours, for the Lundum Dare 31 Competition (2014-12-06).

![Music Screenshot](http://nilspferd.net/ld31/screenshot.png)

*Theme: Entire Game on One Screen*

# Downloads

* [Play in Browser or Download for Windows, OSX, Linux](http://gamejolt.com/games/puzzle/music/40958/)

# Stuff used 

* Engine: Unity 2D
* Gfx: Affinity Designer
* Sfx: Garage Band & Audacity

2014 by Nils Kübler

http://artwaretists.com
