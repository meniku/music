﻿using UnityEngine;
using System.Collections.Generic;

public class Game : MonoBehaviour {
	public static bool solve = false;

	public AudioClip dragSound;
	public AudioClip dropSound;
	public AudioClip revealSound;

	public Camera theCamera;

	public GameObject credits;

	public Element[] elements;
	public Dictionary<Element, int> dottedElements = new Dictionary<Element, int>();

	public float dotSpeed = 0.058f;
	public float activateDelay = 0.08f;
	public int maxDots = 4;

	private float threshold = 1f;
	private bool dragging = false;
	private Element mouseDownElement;
	private Vector2 mouseDragStartPosition;
	private Vector3 mouseDragStartElementPosition;

	private bool running = false;

	void Start() {
		if(!solve) {
			for(int i = 2; i< elements.Length; i++) {
				elements[i].gameObject.SetActive(false);
			}
		}
	}

	public void MouseDownOnElement(Element element) {
		collider2D.enabled = true;
		if(!dragging) {
			mouseDownElement = element;
			mouseDragStartPosition = Input.mousePosition;
			mouseDragStartElementPosition = element.transform.position;
		}
	}


	public void MouseUpOnElement(Element element) {
		OnMouseUp();
	}

	void Update() {
		if(mouseDownElement != null) {
			if(!dragging && ((Vector2)Input.mousePosition - mouseDragStartPosition).magnitude > threshold) {
				startDrag();
			}
			if(dragging) {
				updateDrag();
			}
		}

		if(running) {
			GameObject dot = GameObject.FindGameObjectWithTag("Dot");
			if(dot == null) {
				running = false;
				BroadcastMessage("reset");
				checkSuccess();
			}
		} else {
			GameObject dot = GameObject.FindGameObjectWithTag("Dot");
			if(dot != null) {
				running = true;
			}
		}
	}

	private void checkSuccess() {
		if(solve) {
			return;
		}
		bool success = true;
		foreach(Element element in this.elements) {
			if(element.gameObject.activeInHierarchy) {
				if(!dottedElements.ContainsKey(element)) {
					success = false;
				} else {
//					if(element is Timer) {
//						if(!((Timer)element).isReset()) {
//							success = false;
//						}
//					}
				}
			}
		}
		
		if(success) {
			nextLevel();
		}
	}

	private void nextLevel() {
		int activateNumber = 1;
		foreach(Element element in this.elements) {
			if(!element.gameObject.activeInHierarchy) {
				element.gameObject.SetActive(true);
				element.gameObject.transform.localScale = new Vector3(0.01f, 0.01f, 1.0f);
				LeanTween.scale(element.gameObject, new Vector2(1f, 1f), 0.5f).setEase(LeanTweenType.easeOutQuad);
				element.transform.position = getSpawnPos();
				if(element is Duplicator || element is Emitter) {
					continue;
				}
				if(--activateNumber == 0) {
					audio.PlayOneShot(revealSound);
					return;
				}
			}
		}

		dottedElements.Clear();
		credits.SetActive(true);
	}
	
	public void OnMouseDown() {
		Debug.Log ("Mouse Down On Background");
	}

	void OnMouseUp() {
		collider2D.enabled = false;
		if(mouseDownElement != null) {
			if(!dragging) {
				dottedElements.Clear();
				BroadcastMessage("reset");
				GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Dot");
				foreach(GameObject gameObject in gameObjects) {
					Destroy(gameObject);
				}
				if(mouseDownElement == elements[0]) {
					dottedElements.Add(elements[0], 1);
				}
				if(mouseDownElement.dotEnabled) {
					mouseDownElement.Activate();
				}
			} else {
				stopDrag();
			}
			mouseDownElement = null;
		}
	}
	
	private void startDrag() {
		dragging = true;
		mouseDownElement.StartDrag();
		audio.PlayOneShot(dragSound);
	}
	
	private void stopDrag() {
		dragging = false;
		mouseDownElement.StopDrag();
		audio.PlayOneShot(dropSound);
	}

	private void updateDrag() {
		Vector3 newPos = mouseDragStartElementPosition + (theCamera.ScreenToWorldPoint(Input.mousePosition) - theCamera.ScreenToWorldPoint(mouseDragStartPosition));
//		newPos.x = Mathf.Round(newPos.x * 1f + 0.5f) / 1f - 0.5f;
//		newPos.y = Mathf.Round(newPos.y * 1f + 0.5f) / 1f - 0.5f;
		newPos.x = Mathf.Round(newPos.x * 2f) / 2f;
		newPos.y = Mathf.Round(newPos.y * 2f) / 2f;
		newPos.x = Mathf.Min (newPos.x, 3.5f);
		newPos.y = Mathf.Min (newPos.y, 2.5f);
		newPos.x = Mathf.Max (newPos.x, -3.5f);
		newPos.y = Mathf.Max (newPos.y, -2.5f);
		if((mouseDownElement.transform.position - newPos).magnitude > 0.1f) {
			if(isAvailable(newPos)) {
				mouseDownElement.transform.position = newPos;
			}
		}
	}

	private Vector2 getSpawnPos() {
		Vector2 currentPos = new Vector2(0f, 2.5f);
		while(!isAvailable(currentPos)) {
			if(currentPos.x >= 3.5f) {
				currentPos = new Vector2(-3.5f, currentPos.y - 0.5f);
			} else {
				currentPos.x += 0.5f;
			}
		}
		return currentPos;
	}

	private bool isAvailable (Vector2 newPos) {
		foreach(Element element in this.elements) {
			if(((Vector2)element.transform.position - newPos).magnitude < 0.1f) {
				return false;
			}
		}
		return true;
	}

	public void elementActivatedViaDot(Element element) {
		int count = 1;
		if(dottedElements.ContainsKey(element)) {
			count += dottedElements[element];
			dottedElements.Remove(element);
		}
		dottedElements.Add(element, 1);
		checkSuccess();
	}
}
