﻿using UnityEngine;
using System.Collections;

public class Duplicator : Element {

	public Vector2 direction1;
	public Vector2 direction2;

	override public void Activate() {
		this.flash();
		this.emitDot(direction1);
		this.emitDot(direction2);
	}
}
