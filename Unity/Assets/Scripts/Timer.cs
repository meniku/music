﻿using UnityEngine;
using System.Collections;

public class Timer : Element {
	
	public Vector2 direction;

	private int startNumber;
	public int number;

	public Sprite[] numbers;

	public GameObject numberSprite;

	private bool tmpDisbabled;

	public bool isReset() { 
		return !tmpDisbabled && number == startNumber;
	}

	override protected void Start() {
		base.Start();
		startNumber = number;
	}
	
	override public void Activate() {
		if(tmpDisbabled) {
			reset ();
		} else {
			flash();

			number --;
			if(number >= 0) {
				((SpriteRenderer)numberSprite.gameObject.renderer).sprite = numbers[number];
				if(number == 0) {
					this.disable();
				} 
				this.emitDot(direction);
			}
		}
	}

	override public void reset() {
		this.number = startNumber;
		((SpriteRenderer)numberSprite.gameObject.renderer).sprite = numbers[number];
		this.enable();
	}

	
	protected new void disable() {
		((SpriteRenderer)this.renderer).color = Color.gray;
		colorize(Color.gray);
		tmpDisbabled = true;
//		destroyDotOnActivate = false;
	}
	
	protected new void enable() {
		((SpriteRenderer)this.renderer).color = Color.white;
		restoreColors();
		tmpDisbabled = false;
//		destroyDotOnActivate = true; 
	}
}
