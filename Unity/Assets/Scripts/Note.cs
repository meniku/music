﻿using UnityEngine;
using System.Collections;

public class Note : Element {

	public AudioClip sound;

	public Vector2 direction;

	public GameObject particles;

	override public void Activate() {
		transform.parent.audio.PlayOneShot(sound);
		emitDot(direction);
		flash();
		particles.gameObject.SetActive(false);
		particles.gameObject.SetActive(true);
	}

}
