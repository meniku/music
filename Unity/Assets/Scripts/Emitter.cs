﻿using UnityEngine;
using System.Collections;

public class Emitter : Element {

	public Vector2 direction;

	override public void Activate() {

		emitDot(direction);

		flash();
	}
}
