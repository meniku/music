﻿using UnityEngine;
using System.Collections;

public class Dot : MonoBehaviour {
	public Vector2 direction = new Vector2(0f, 0f);

	public int age = 0;

	private float rot = 0.0f;

	private Game game;
	
	void Start() {
		game = GameObject.FindGameObjectWithTag("Game").GetComponent<Game>();
	}

	void FixedUpdate() {
		age++;
		Vector3 pos = transform.position;
		pos.x += direction.x * game.dotSpeed;
		pos.y += direction.y * game.dotSpeed;
		transform.position = pos;

		if(pos.x >= 5.0f || pos.y >= 5.0f || pos.x <= -5f || pos.y <= -5f) {
			Destroy(this.gameObject);
		}
		rot += 4f;
		transform.localRotation = Quaternion.Euler(0f,0f,rot);
	}
}
