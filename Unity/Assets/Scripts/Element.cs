﻿using UnityEngine;
using System.Collections;

public abstract class Element : MonoBehaviour {

	public GameObject[] foreground;
	private Color[] previousColors;
	
	public Dot dotPrefab;

	private int ignoreNext = 0;

	[HideInInspector]
	public bool dotEnabled = true;

	public abstract void Activate();

	private Game game;

//	protected bool destroyDotOnActivate = true;

	private Dot currentDot;
	
	virtual protected void Start() {
		game = GetComponentInParent<Game>();
		previousColors = new Color[foreground.Length];
		for (int i = 0; i < previousColors.Length; i++) {
			previousColors[i] = ((SpriteRenderer)this.foreground [i].renderer).color;
		}
	}

	void OnMouseDown() {
		game.MouseDownOnElement(this);
	}

	void OnMouseUp() {
		game.MouseUpOnElement(this);
	}

	void OnTriggerEnter2D(Collider2D otherCollider) {
		if(ignoreNext > 0) {
			ignoreNext--;
			return;
		}
		if(dotEnabled && otherCollider.gameObject.tag == "Dot") {
			StartCoroutine(activateDelayed(otherCollider.gameObject));
		}
	}

	private IEnumerator activateDelayed(GameObject gameObject) {
		yield return new WaitForSeconds(GetComponentInParent<Game>().activateDelay);
		if(gameObject != null && gameObject.activeInHierarchy) {
//			if(destroyDotOnActivate) {
//				GameObject.Destroy(gameObject);
				currentDot = gameObject.GetComponent<Dot>();
//			}
			Activate();
			currentDot = null;
			game.elementActivatedViaDot(this);
		}
	}

	protected void flash() {
		StartCoroutine(flashCoroutine());
	}

	private IEnumerator flashCoroutine() {
		colorize (Color.white);
		yield return new WaitForSeconds(0.1f);
		restoreColors();
	}

	protected void emitDot(Vector2 direction) {
		if(currentDot == null) {
			ignoreNext++;
			GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Dot");
			int deleteCount = gameObjects.Length - (game.maxDots - 1);
			for(int i = 0; i < deleteCount; i++) {
				deleteOldestDot();
			}
			Dot dot = (Dot)GameObject.Instantiate(dotPrefab, transform.position, Quaternion.identity);
			dot.direction = direction;
		} else {
			currentDot.direction = direction;
			currentDot = null;
		}
	}

	private void deleteOldestDot() {
		Dot deleteDot = null;
		GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("Dot");
		for (int i = 0 ; i < gameObjects.Length; i++) {
			Dot dot = gameObjects[i].GetComponent<Dot>();
			if(deleteDot == null || (dot != null && dot.age > deleteDot.age)) {
				deleteDot = dot;
			}
		}
		Destroy(deleteDot.gameObject);
	}

	public void StartDrag() {
		((SpriteRenderer)this.renderer).color = Color.gray;
		colorize(Color.gray);
	}

	public void StopDrag() {
		((SpriteRenderer)this.renderer).color = Color.white;
		restoreColors();
	}

	public virtual void reset() {
	}
	
	protected void colorize (Color color)
	{
		for (int i = 0; i < foreground.Length; i++) {
			((SpriteRenderer)this.foreground [i].renderer).color = color;
		}
	}
	
	protected void restoreColors() {
		for(int i=0; i< foreground.Length; i++) {
			((SpriteRenderer)this.foreground[i].renderer).color = previousColors[i];
		}
	}

	protected void disable() {
		((SpriteRenderer)this.renderer).color = Color.gray;
		colorize(Color.gray);
		this.dotEnabled = false;
	}
	
	protected void enable() {
		((SpriteRenderer)this.renderer).color = Color.white;
		restoreColors();
		this.dotEnabled = true;
	}
}
